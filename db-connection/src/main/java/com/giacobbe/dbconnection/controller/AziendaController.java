package com.giacobbe.dbconnection.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import com.giacobbe.dbconnection.model.Azienda;
import com.giacobbe.dbconnection.repository.AziendaRepository;

@CrossOrigin(origins = "*")
@RestController
public class AziendaController {
	@Autowired
	private AziendaRepository repository;

	@GetMapping("/aziende")
	public List<Azienda> all() {
		return repository.findAll();
	}

	@PostMapping("/aziende")
	public Azienda inserisci(@Valid @RequestBody Azienda az) {
		return repository.save(az);
	}

	@PutMapping("/aziende/{id}")
	public Azienda modifica(@RequestBody Azienda az, @PathVariable Integer id) {

		repository.findById(id).ifPresentOrElse((d) -> {
			d.setNome(az.getNome());
			d.setIndirizzo(az.getIndirizzo());
			repository.save(d);
		}, () -> {
			repository.save(az);

		});
		return repository.findById(id).get();
	}
}
