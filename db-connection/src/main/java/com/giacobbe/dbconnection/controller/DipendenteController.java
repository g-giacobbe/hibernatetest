package com.giacobbe.dbconnection.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.giacobbe.dbconnection.exception.ResourceNotFoundException;
import com.giacobbe.dbconnection.model.Dipendente;
import com.giacobbe.dbconnection.repository.AziendaRepository;
import com.giacobbe.dbconnection.repository.DipendenteRepository;
import javax.validation.Valid;

@CrossOrigin(origins = "*")
@RestController
public class DipendenteController {
	@Autowired
	private DipendenteRepository repository;
	
	
	@Autowired
	private AziendaRepository aziendaRepository;

//@PostMapping("/utenti/{id}")

	@GetMapping("/dipendenti")
	public List<Dipendente> all() {
		return repository.findAll();
	}
	/*
	@GetMapping("/aziende/{aziendaId}/dipendenti")
	public Page<Dipendente> getAllDipByAzId(@PathVariable (value = "aziendaId") Integer aziendaId,
											Pageable pageable) {
		return repository.findByAziendaId(aziendaId, pageable);
	}
*/	

	@GetMapping("/aziende/{aziendaId}/dipendenti")
	public List<Dipendente> getAllDipendentiByAziendaId(@PathVariable (value = "aziendaId") Integer aziendaId,
											List list) {
		return repository.findByAziendaId(aziendaId, list);
	}
/*
	@PostMapping("/utenti")
	public Dipendente inserisci(@RequestBody Dipendente dipendente) {
		return repository.save(dipendente);
	}
*/

@PostMapping("/aziende/{aziendaId}/dipendenti")
public Dipendente inserisciDipendente(@PathVariable (value= "aziendaId") Integer aziendaId,
									  @Valid @RequestBody Dipendente dipendente) {
	return aziendaRepository.findById(aziendaId).map(azienda -> {
		dipendente.setAzienda(azienda);
		return repository.save(dipendente);
	}).orElseThrow(() -> new ResourceNotFoundException("aziendaId" + aziendaId + "not found"));
	
	}
	
	

	
	
	
	@PutMapping("/utenti/{id}")
	public Dipendente aggiorna(@RequestBody Dipendente dipendente, @PathVariable Integer id) {
		repository.findById(id).ifPresentOrElse((u) -> {
			u.setPassword(dipendente.getPassword());
			u.setUsername(dipendente.getUsername());
			repository.save(u);
		}, () -> {
			repository.save(dipendente);
		});
		return repository.findById(id).get();

	}

	@DeleteMapping("/utenti/{id}")
	public void elimina(@PathVariable Integer id) {
		repository.deleteById(id);
	}
	
	
}
