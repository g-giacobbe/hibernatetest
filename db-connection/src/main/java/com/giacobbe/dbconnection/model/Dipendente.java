package com.giacobbe.dbconnection.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.giacobbe.dbconnection.model.Azienda;

@Entity
public class Dipendente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int dipendenteId;

	@Column(length = 20)
	private String username;

	public int getDipendenteId() {
		return dipendenteId;
	}

	public void setDipendenteId(int dipendenteId) {
		this.dipendenteId = dipendenteId;
	}

	@Column(nullable = false, length = 20)
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/*
	 * @Column(nullable = false, length = 20)
	 * 
	 * @OneToOne
	 * 
	 * @JoinColumn(name = "azienda_aziendaId") private Azienda azienda;
	 * 
	 * 
	 * @OneToMany(mappedBy = "utente") private List<Azienda> azienda;
	 * 
	 */
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "aziendaId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private Azienda azienda;

	public Azienda getAzienda() {
		return azienda;
	}

	public void setAzienda(Azienda azienda) {
		this.azienda = azienda;
	}

	@Override
	public String toString() {
		return "Dipendente [dipendenteId=" + dipendenteId + ", username=" + username + ", password=" + password
				+ ", azienda=" + azienda + "]";
	}	
	
	



}
