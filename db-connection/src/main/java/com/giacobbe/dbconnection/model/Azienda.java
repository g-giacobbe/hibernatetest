package com.giacobbe.dbconnection.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Azienda {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int aziendaId;
	
	@Column(length = 20)
	private String nome;
	
	@Column(nullable = false, length = 20)
	private String indirizzo;
	
	

	public int getAziendaId() {
		return aziendaId;
	}

	public void setAziendaId(int aziendaId) {
		this.aziendaId = aziendaId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	@Override
	public String toString() {
		return "Azienda [aziendaId=" + aziendaId + ", nome=" + nome + ", indirizzo=" + indirizzo + "]";
	}

}
