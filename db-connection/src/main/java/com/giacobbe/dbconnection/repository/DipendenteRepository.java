package com.giacobbe.dbconnection.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giacobbe.dbconnection.model.Dipendente;

public interface DipendenteRepository extends JpaRepository<Dipendente, Integer> {
	List<Dipendente> findByAziendaId(Integer aziendaId, List list);

}
