package com.giacobbe.dbconnection.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.giacobbe.dbconnection.model.Azienda;

public interface AziendaRepository extends JpaRepository<Azienda, Integer> {
}
